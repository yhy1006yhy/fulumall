package com.yhy.fulumail.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FulumailCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulumailCouponApplication.class, args);
    }

}
