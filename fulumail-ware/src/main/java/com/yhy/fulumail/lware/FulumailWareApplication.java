package com.yhy.fulumail.lware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FulumailWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulumailWareApplication.class, args);
    }

}
