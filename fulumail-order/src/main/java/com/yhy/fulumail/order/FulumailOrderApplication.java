package com.yhy.fulumail.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FulumailOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulumailOrderApplication.class, args);
    }

}
