package com.yhy.fulumail.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FulumailMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulumailMemberApplication.class, args);
    }

}
