package com.yhy.fulumail.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FulumailProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulumailProductApplication.class, args);
    }

}
